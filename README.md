# react-laravel-with-jwt-authentication
FantasyLab Company site using reactjs (15.6), reducer, sass, laravel (5.8), mysql with jwt-authentication.

# usage :
1. Rename .env.example to .env
2. Update DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD, and Social IDs in .env file to yours.
3. In command line : 
   - composer update
   - Php artisan key:generate
   - php artisan migrate
4. npm install
5. npm run dev
6. php artisan serve

# To-Do list :
- User Authentication
- Forgot password
- Limit login attempts
- Social Login
- CMS
- Babel Webpack 

# The hardest problem 
It was Internet explorer doesn't render react in laravel
I spent more than 30 hours for finding the right solution.
There are lots of same issue articles on github and stackoverflow, but it was so hard to find the correct solution.
Finally, I solved with adding babel configuration and using their correct version in combination.

# Demo Site
- [staging] https://fantasy.fantasylab.io
- [live] https://fantasylab.io